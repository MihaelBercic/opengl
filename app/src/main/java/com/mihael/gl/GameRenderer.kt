package com.mihael.gl

import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import android.util.Log
import java.nio.ByteBuffer
import java.nio.ByteOrder
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class GameRenderer : GLSurfaceView.Renderer {


    // Particles
    private val pointsArray = Array(10000) { Particle() }

    // Destination for points to move towards
    var desiredDestination: Vector? = Vector(0f, 0f)
        set(value) {
            pointsArray.forEach { it.desiredDestination = value }
            field = value
        }

    // OpenGL object ids
    private var programId = 0
    private var vertexId = 0
    private var fragmentId = 0

    // Shader ids
    private var positionLocation = -1
    private var colorLocation = -1

    private val dataBuffer = ByteBuffer
        .allocateDirect(pointsArray.size * 8)
        .order(ByteOrder.nativeOrder())
        .asFloatBuffer()


    override fun onDrawFrame(gl: GL10?) {
        gl?.glClear(GL_COLOR_BUFFER_BIT);
        dataBuffer.clear()
        pointsArray.forEach { it.move() }

        dataBuffer.put(pointsArray.flatMap { it.position.asArray.asList() }.toFloatArray())
        dataBuffer.position(0)
        glDrawArrays(GL_POINTS, 0, pointsArray.size)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        gl?.glViewport(0, 0, width, height);
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        gl?.apply {
            glClearColor(0f, 0f, 0f, 1f)
            val status = intArrayOf(-1)

            // Load, compile and validate shaders. Set their object IDs
            vertexId = loadShader(GL_VERTEX_SHADER, vertexShaderCode)
            fragmentId = loadShader(GL_FRAGMENT_SHADER, fragmentShaderCode)
            programId = glCreateProgram()

            // Attaching shaders and linking the GL Program
            glAttachShader(programId, vertexId)
            glAttachShader(programId, fragmentId)
            glLinkProgram(programId)
            glGetProgramiv(programId, GL_LINK_STATUS, status, 0)

            if (status[0] == 0) {
                Log.v("DEBUG", "Linking failed")
                return
            }

            glUseProgram(programId)

            positionLocation = glGetAttribLocation(programId, "a_Position")
            colorLocation = glGetUniformLocation(programId, "u_Color")

            glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, dataBuffer)
            glEnableVertexAttribArray(positionLocation)

            // Particle color
            glUniform4f(colorLocation, .3f, .3f, 1f, 1f)


            if (DEBUG) {
                Log.d("DEBUG", "Program id: $programId")
                Log.d("DEBUG", "\nPosition location: $positionLocation")
                Log.d("DEBUG", "Color location: $colorLocation")
            }
        }
    }


}

private const val vertexShaderCode = "attribute vec4 a_Position; void main() { gl_PointSize = 5.0; gl_Position = a_Position;}"
private const val fragmentShaderCode = "precision mediump float;uniform vec4 u_Color;void main() {gl_FragColor = u_Color;}"

fun loadShader(type: Int, shaderCode: String?): Int {
    val shader = glCreateShader(type)
    val status = intArrayOf(0)
    glShaderSource(shader, shaderCode)
    glCompileShader(shader)
    glGetShaderiv(shader, GL_COMPILE_STATUS, status, 0)
    Log.d("DEBUG", "Shader compile status: ${status[0]}")
    return shader
}