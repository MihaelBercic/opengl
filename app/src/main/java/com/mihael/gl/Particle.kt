package com.mihael.gl

import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

const val range = 0.005f
val randomSmallFloat get() = Random.nextDouble(-range.toDouble(), range.toDouble()).toFloat()
val randomPositionFloat get() = Random.nextDouble(-1.0, 1.0).toFloat()

data class Particle(
    var position: Vector = Vector(randomPositionFloat, randomPositionFloat),
    var speed: Vector = Vector(randomSmallFloat, randomSmallFloat),
    var desiredDestination: Vector? = null,
    var reset: Boolean = false
) {
    fun move() {
        desiredDestination?.also {
            reset = false
            val distance = it.distanceTo(position)
            val isCloseEnough = distance <= 0.2
            val speedX = it.x - position.x
            val speedY = it.y - position.y

            speed.x = if (isCloseEnough) position.x - it.x else speedX * distance / Random.nextInt(1, 500)
            speed.y = if (isCloseEnough) position.y - it.y else speedY * distance / Random.nextInt(1, 500)

        } ?: if (!reset) {
            speed.x = randomSmallFloat
            speed.y = randomSmallFloat
            reset = true
        }

        position.add(speed)

        // Collision
        if (position.x >= 1 || position.x <= -1) speed.x *= -0.8f
        if (position.y >= 1 || position.y <= -1) speed.y *= -0.8f
    }
}

data class Vector(var x: Float, var y: Float) {

    fun add(v: Vector) {
        x += v.x
        y += v.y
    }

    val asArray get() = arrayOf(x, y)
    val size get() = sqrt(x * x + y * y)
    fun distanceTo(v: Vector) = sqrt((x - v.x).pow(2) + (y - v.y).pow(2))
}

