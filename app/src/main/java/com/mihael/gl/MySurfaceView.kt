package com.mihael.gl

import android.annotation.SuppressLint
import android.content.Context
import android.opengl.GLSurfaceView
import android.view.MotionEvent

@SuppressLint("ViewConstructor")
class MySurfaceView(context: Context, private val gameRenderer: GameRenderer) : GLSurfaceView(context) {

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_MOVE,
            MotionEvent.ACTION_DOWN -> {
                val x = ((event.x / width) * 2 - 1)
                val y = -((event.y / height) * 2 - 1)
                gameRenderer.desiredDestination = Vector(x, y)
            }
            MotionEvent.ACTION_UP -> gameRenderer.desiredDestination = null
        }
        return true
    }

}