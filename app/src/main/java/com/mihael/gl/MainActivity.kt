package com.mihael.gl

import android.app.ActivityManager
import android.content.Context
import android.content.pm.ConfigurationInfo
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity


const val DEBUG = false

class MainActivity : AppCompatActivity() {

    private val gameRenderer = GameRenderer()

    private lateinit var surfaceView: GLSurfaceView
    private lateinit var activityManager: ActivityManager
    private lateinit var configurationInfo: ConfigurationInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        surfaceView = MySurfaceView(this, gameRenderer)
        activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        configurationInfo = activityManager.deviceConfigurationInfo

        if (configurationInfo.reqGlEsVersion >= 0x20000) surfaceView.apply {
            setEGLContextClientVersion(2)
            setRenderer(gameRenderer)
            setContentView(this)
        } else Log.d("DEBUG", "No OpenGL ES 2.0")
    }
}

